# Árvore Rubro-Negra com persistencia parcial

## Davi Tabosa e Jaciana

### Requisitos

- Python com versão 3.8 ou mais recente

### Como executar

- Faça o clone deste repositório
- Crie um arquivo .txt com a sequencia de operações. As operações devem ser separadas por nova linha '/n'.
- execute o comando
```sh
python3 main.py comandos.txt
```
Ou se utilizar Windows:
```sh
python main.py comandos.txt
```
### Problemas
- Funcionalidade de remoção de nó não funciona como deveria.