import sys
from arn import RBTree, RBNode, insert as arn_insert, search as arn_search, print_tree, delete as arn_delete, sucessor as arn_sucessor
from arn_parser import Parser, Command, Instruction

if __name__ == "__main__" or True:
    path = sys.argv[1] # second arg should be the path
    p = Parser(path)
    instructions = p.get_commands()
    tree = RBTree()
    for instruction in instructions:
        print(instruction)
        if instruction.command == Command.INCLUDE:
            node = RBNode(key=int(instruction.args[0]))
            arn_insert(tree, node)
        elif instruction.command == Command.REMOVE:
            node = arn_search(tree, int(instruction.args[0]), tree.version)
            arn_delete(tree, node)
        elif instruction.command == Command.SUCCESSOR:
            value = int(instruction.args[0])
            version = int(instruction.args[1])
            node = arn_sucessor(tree, value, version)
            print(node.key)
        elif instruction.command == Command.PRINT:
            print_tree(tree, instruction.args[0])
        else:
            print(f"Command {instruction.command} not implemented.")
