from copy import copy
from enum import Enum, auto
from typing import Union, List, Any
from dataclasses import dataclass

class Color(Enum):
    RED = "R"
    BLACK = "B"


@dataclass
class RBNodeMod:
    key: Any
    left: 'RBNode'
    right: 'RBNode'
    parent: 'RBNode'
    color: Color
    version: int
    def __repr__(self):
        return f"{self.key} {self.color.value} version={self.version} right={self.right} left={self.left}"

class RBNode:
    def __init__(self, left=None, right=None, parent=None, color=Color.RED, key=None) -> None:
        self.key = key
        self._left: RBNode = left
        self._right: RBNode = right
        self._parent: RBNode = parent
        self._color: Color = color
        self._current_mod: RBNodeMod = RBNodeMod(key, self._left, self._right, self._parent, color, -1)
        self.mods: List[RBNodeMod] = [None] * 100

    def get_in_version(self, version: int) -> 'RBNode':
        node = self
        latest_version = -1
        for mod in self.mods:
            if mod and mod.version <= version:
                latest_version = mod.version
                latest_mod = mod
        if latest_version > -1:
            left = latest_mod.left.get_in_version(version)
            right = latest_mod.right.get_in_version(version)
            node = RBNode(left, right, latest_mod.parent, latest_mod.color, latest_mod.key)
        return node

    def copy(self):
        copy_node = RBNode()
        copy_node._left = self.left
        copy_node._right = self.right
        copy_node._parent = self.parent
        copy_node._color = self.color
        copy_node.key = self.key
        return copy_node

    @property
    def left(self):
        left_recent = self._current_mod.left
        return left_recent

    @left.setter
    def left(self, new_left: 'RBNode'):
        self._current_mod.left = new_left

    @property
    def right(self):
        right_recent = self._current_mod.right
        return right_recent

    @right.setter
    def right(self, new_right: 'RBNode'):
        self._current_mod.right = new_right

    @property
    def parent(self):
        parent_recent = self._current_mod.parent
        return parent_recent

    @parent.setter
    def parent(self, new_parent):
        self._current_mod.parent = new_parent

    @property
    def color(self):
        color_recent = self._current_mod.color
        return color_recent

    @color.setter
    def color(self, new_color: Color):
        self._current_mod.color = new_color

    def persist(self, version: int):
        """Persists a node into the modifications array

        Parameters
        ----------
        node : RBNode
            The node to persist
        version : int
            The version of the data structure
        """
        new_mod = copy(self._current_mod)
        new_mod.version = version
        idx = 0
        for mod in self.mods:
            if mod is None or mod.version == version:
                break
            idx += 1
        try:
            self.mods[idx] = new_mod
        except IndexError:
            print(f"{self!r} buffer full")
        # else:
        #     self._current_mod = RBNodeMod(None, None, None, None, Color.RED, -1)

    def __str__(self) -> str:
        return f"({self.key} {self._color.value})"

    def __repr__(self):
        return f"{self.key} {self.color.value} right={self.right} left={self.left}"

class RBNil(RBNode):
    """Red-Black tree nil sentinel node"""
    def __init__(self) -> None:
        super().__init__(color=Color.BLACK) # nil nodes are black
        self._left = self
        self._right = self
    def get_in_version(self, version: int) -> 'RBNode':
        return RBNil()
    @property
    def color(self):
        return Color.BLACK
    @color.setter
    def color(self, new_color):
        pass
    def copy(self):
        return RBTree.nil
    def __repr__(self) -> str:
        return "<NIL>"
    def __str__(self) -> str:
        return "<NIL>"
    def __bool__(self):
        return False

@dataclass
class RBTreeMod:
    root: RBNode
    version: int
    mod_version: int

class RBTree:
    """Red-Black tree"""
    nil = RBNil()

    def __init__(self) -> None:
        self._root = RBTree.nil
        self.version = 0
        self.mods: List[RBTreeMod] = [None] * 100
        self._current_mod: RBTreeMod = RBTreeMod(self._root, self.version, self.version)

    @property
    def next_version(self):
        return self.version + 1
    @property
    def root(self):
        root_recent = self._current_mod.root
        return root_recent

    def copy(self):
        copy_tree = RBTree()
        copy_tree._root = self.root
        copy_tree.version = self.version
        return copy_tree

    @root.setter
    def root(self, new_root):
        self._current_mod.root = new_root

    def persist(self, version: int):
        new_mod = copy(self._current_mod)
        new_mod.version = version
        new_mod.mod_version = version
        idx = 0
        for mod in self.mods:
            if mod is None or mod.mod_version == version:
                break
            idx += 1
        try:
            self.mods[idx] = new_mod
        except IndexError:
            print(f"{self!r} buffer full")

    def get_in_version(self, version: int):
        tree = self
        for mod in self.mods:
            if mod and mod.mod_version <= version:
                tree = RBTree()
                tree._root = mod.root.get_in_version(version)
                tree.version = mod.version
        return tree

    def upgrade(self):
        self.version += 1



def search(node: Union[RBTree, RBNode], key, version):
    if isinstance(node, RBTree):
        return search(node.get_in_version(version)._root, key, version)
    nv = node.get_in_version(version)
    if isinstance(nv, RBNil) or key == nv.key:
        return nv
    if key < nv.key:
        return search(nv.left, key, version)
    return search(nv.right, key, version)

def minimum(node: Union[RBTree, RBNode]):
    if node is None:
        return RBNil()
    if isinstance(node, RBTree):
        return minimum(node.root)
    while not isinstance(node.left, RBNil):
        node = node.left
    return node

def maximum(node: Union[RBTree, RBNode]):
    if isinstance(node, RBTree):
        return maximum(node.root)
    while not isinstance(node.left, RBNil):
        node = node.right
    return node

def sucessor(tree: RBTree, value, version):
    node = search(tree, value, version)
    if not node:
        return tree.nil
    if not isinstance(node._right, RBNil):
        return minimum(node._right)
    parent: RBNode = node._parent
    while not isinstance(parent, RBNil) and node == parent._right:
        node = parent
        parent = parent._parent
    return parent

def predecessor(node: Union[RBTree, RBNode], version):
    if isinstance(node, RBTree):
        return predecessor(node.get_in_version(version)._root)
    if node.left != RBTree.nil:
        return maximum(node.left)
    parent: RBNode = node.parent
    while parent != RBTree.nil and node == parent.left:
        node = parent
        parent = parent.parent
    return parent

def insert(tree: RBTree, z: RBNode):
    y = tree.nil
    x = tree.root
    while x != tree.nil:
        y = x
        if z.key < y.key:
            x = x.left
        else:
            x = x.right
    z.parent = y
    if y == tree.nil:
        tree.root = z
        tree.persist(tree.next_version)
    elif z.key < y.key:
        y.left = z
        y.persist(tree.next_version)
    else:
        y.right = z
        y.persist(tree.next_version)
    z.left = tree.nil
    z.right = tree.nil
    z.color = Color.RED
    __fix_insert(tree, z)
    tree.persist(tree.next_version)
    tree.upgrade()


def __fix_insert(tree: RBTree, z: RBNode):
    while z.parent.color == Color.RED:
        if z.parent == z.parent.parent.left:
            y: RBNode = z.parent.parent.right
            if y.color == Color.RED:
                z.parent.color = Color.BLACK
                y.color = Color.BLACK
                z.parent.parent.color = Color.RED
                y.persist(tree.next_version)
                z.parent.parent.persist(tree.next_version)
                z.parent.persist(tree.next_version)
            else:
                if z == z.parent.right:
                    z = z.parent
                    rotate_left(tree, z)
                z.parent.color = Color.BLACK
                z.parent.parent.color = Color.RED
                rotate_right(tree, z.parent.parent)
                z.parent.parent.persist(tree.next_version)
                z.parent.persist(tree.next_version)
        else: # simetria
            y: RBNode = z.parent.parent.left
            if y.color == Color.RED:
                z.parent.color = Color.BLACK
                y.color = Color.BLACK
                z.parent.parent.color = Color.RED
                y.persist(tree.next_version)
                z.parent.parent.persist(tree.next_version)
                z.parent.persist(tree.next_version)
            else:
                if z == z.parent.left:
                    z = z.parent
                    rotate_right(tree, z)
                z.parent.color = Color.BLACK
                z.parent.parent.color = Color.RED
                tree.persist(tree.next_version)
                z.parent.parent.persist(tree.next_version)
                rotate_left(tree, z.parent.parent)
                z.parent.parent.persist(tree.next_version)
                z.parent.persist(tree.next_version)
    tree.root.color = Color.BLACK
    tree.root.persist(tree.next_version)

def __transplant(tree: RBTree, u: RBNode, v: RBNode):
    if u.parent == tree.nil:
        tree.root = v
        tree.persist(tree.next_version)
    elif u == u.parent.left:
        u.parent.left = v
        u.parent.persist(tree.next_version)
        u.persist(tree.next_version)
    else:
        u.parent.right = v
        u.parent.persist(tree.next_version)
        u.persist(tree.next_version)
    v.parent = u.parent
    v.persist(tree.next_version)

def delete(tree: RBTree, z: RBNode):
    y: RBNode = z
    y_original_color = y.color
    if z.left == tree.nil:
        x: RBNode = z.right
        __transplant(tree, z, z.right)
    elif z.right == tree.nil:
        x = z.left
        __transplant(tree, z, z.left)
    else:
        y = minimum(z.right)
        y_original_color = y.color
        x = y.right
        if y.parent == z:
            x.parent = y
            x.persist(tree.next_version)
        else:
            __transplant(tree, y, y.right)
            y.right = z.right
            y.persist(tree.next_version)
            y.right.parent = y
            y.right.parent.persist(tree.next_version)
            y.right.persist(tree.next_version)
            y.persist(tree.next_version)
        __transplant(tree, z, y)
        y.left = z.left
        y.persist(tree.next_version)
        y.left.parent = y
        y.left.persist(tree.next_version)
        y.color = z.color
        y.persist(tree.next_version)
    if y_original_color == Color.BLACK:
        __fix_delete(tree, x)
    tree.persist(tree.next_version)
    tree.upgrade()

def __fix_delete(tree: RBTree, x: RBNode):
    while x != tree.root and x.color == Color.BLACK:
        if x == x.parent.left:
            w: RBNode = x.parent.right
            if w.color == Color.RED:
                w.color = Color.BLACK
                w.persist(tree.next_version)
                x.parent.color = Color.RED
                x.parent.persist(tree.next_version)
                x.persist(tree.next_version)
                rotate_left(tree, x.parent)
                w = x.parent.right
            if w.left.color == Color.BLACK and w.right.color == Color.BLACK:
                w.color = Color.RED
                w.persist(tree.next_version)
                x.persist(tree.next_version)
                x = x.parent
            else:
                if w.right.color == Color.BLACK:
                    w.left.color = Color.BLACK
                    w.left.persist(tree.next_version)
                    w.color = Color.RED
                    w.persist(tree.next_version)
                    rotate_right(tree, w)
                    w = x.parent.right
                w.color = x.parent.color
                w.persist(tree.next_version)
                x.parent.color = Color.BLACK
                x.parent.persist(tree.next_version)
                x.persist(tree.next_version)
                w.right.color = Color.BLACK
                w.right.persist(tree.next_version)
                w.persist(tree.next_version)
                rotate_left(tree, x.parent)
                x = tree.root
        else:
            w: RBNode = x.parent.left
            if w.color == Color.RED:
                w.color = Color.BLACK
                w.persist(tree.next_version)
                x.parent.color = Color.RED
                x.parent.persist(tree.next_version)
                x.persist(tree.next_version)
                rotate_right(tree, x.parent)
                w = x.parent.left
            if w.right.color == Color.BLACK and w.left.color == Color.BLACK:
                w.color = Color.RED
                w.persist(tree.next_version)
                x = x.parent
            else:
                if w.left.color == Color.BLACK:
                    w.right.color = Color.BLACK
                    w.right.persist(tree.next_version)
                    w.color = Color.RED
                    w.persist(tree.next_version)
                    rotate_left(tree, w)
                    w = x.parent.left
                w.color = x.parent.color
                w.persist(tree.next_version)
                x.parent.color = Color.BLACK
                x.parent.persist(tree.next_version)
                x.persist(tree.next_version)
                w.left.color = Color.BLACK
                w.left.persist(tree.next_version)
                w.persist(tree.next_version)
                rotate_right(tree, x.parent)
                x = tree.root
    x.color = Color.BLACK
    x.persist(tree.next_version)



def print_tree(tree: RBTree, version):
    version = int(version)
    def __print_node(node: RBNode, version, depth=0):
        if node:
            __print_node(node._left, version, depth=depth + 1)
            print(f"{node.key},{depth},{node._color.value}", end=" ")
            __print_node(node._right, version, depth=depth + 1)
    __print_node(tree.get_in_version(version)._root, version)
    print()


def rotate_left(tree_node: RBTree, x: RBNode):
    y: RBNode = x.right
    x.persist(tree_node.next_version)
    x.right = y.left
    if y.left != RBTree.nil:
        y.left.parent = x
        y.left.persist(tree_node.next_version)
    y.parent = x.parent
    if x.parent == RBTree.nil:
        tree_node.root = y
        tree_node.persist(tree_node.next_version)
    else:
        if x == x.parent.left:
            x.parent.left = y
        else:
            x.parent.right = y
        x.parent.persist(tree_node.next_version)
        x.persist(tree_node.next_version)
    y.left = x
    y.persist(tree_node.next_version)
    x.parent = y
    x.persist(tree_node.next_version)

def rotate_right(tree_node: RBTree, x: RBNode):
    y: RBNode = x.left
    x.left = y.right
    if y.right != RBTree.nil:
        y.right.parent = x
        y.right.persist(tree_node.next_version)
    y.parent = x.parent
    if x.parent == RBTree.nil:
        tree_node.root = y
        tree_node.persist(tree_node.next_version)
    else:
        if x == x.parent.right:
            x.parent.right = y
        else:
            x.parent.left = y
        x.parent.persist(tree_node.next_version)
        x.persist(tree_node.next_version)
    y.right = x
    y.persist(tree_node.next_version)
    x.parent = y
    x.persist(tree_node.next_version)
