from typing import List
from arn import RBTree, RBNode
from dataclasses import dataclass, field
from enum import Enum
class Command(Enum):
    INCLUDE = "INC"
    REMOVE = "REM"
    SUCCESSOR = "SUC"
    PRINT = "IMP"

@dataclass
class Instruction:
    command: Command
    args: list = field(default_factory=list)

    def __str__(self) -> str:
        s = self.command.value
        for arg in self.args:
            s += f" {arg}"
        return s

class Parser:
    def __init__(self, path: str) -> None:
        self.buffer = []
        with open(path) as file:
            self.buffer = file.readlines()
        for i, line in enumerate(self.buffer):
            self.buffer[i] = self.buffer[i].strip()
        print(f"Initialized with {len(self.buffer)} instructions.")
    def get_commands(self) -> List[Instruction]:
        instructions = []
        for line in self.buffer:
            splitted_line = line.split(" ")
            cmd = splitted_line[0]
            args = splitted_line[1:]
            instructions.append(Instruction(Command(cmd), args))
        return instructions

